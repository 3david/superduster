$.widget 'ui.googleMap',
  options:
    allowDrawing: false
    defaultPolygonOptions:
      strokeColor: "red"
      strokeOpacity: 0.8
      strokeWeight: 2
      fillColor: "#FF0000"
      fillOpacity: 0.35

  _create: ->
    @polygonsById = {}
    @initMap()

  initMap: ->
    mapOptions =
      center: new google.maps.LatLng(24.886436490787712, -70.2685546875)
      zoom: 5
      mapTypeId: google.maps.MapTypeId.TERRAIN
    @map = new google.maps.Map(@element[0], mapOptions)
    @initDrawingManager() if @options.allowDrawing

    button = $('<a>').attr 'class': 'small icon icons-fullscreen', href: '#'
    button.click =>
      @toggleFullscreen()
      false

    @map.controls[google.maps.ControlPosition.TOP_RIGHT].push(button[0])

  initDrawingManager: ->
    @drawingManager = new google.maps.drawing.DrawingManager
      drawingControlOptions:
        drawingModes: [google.maps.drawing.OverlayType.POLYGON]

      polygonOptions:
        editable: true
    @drawingManager.setMap(@map)

    # TODO: move somewhere else
    google.maps.event.addListener @drawingManager, 'polygoncomplete', (polygon) ->
      value = Geometry.Wkt.fromGooglePolygon(polygon)
      $('#order_area').val(value)

  addPolygonFromWkt: (polygonWkt, id = null) ->
    paths = Geometry.GooglePolygonPaths.fromWkt(polygonWkt)
    @addPolygonFromGooglePaths(paths, id)

  addPolygonFromJson: (polygonJson, id = null) ->
    paths = Geometry.GooglePolygonPaths.fromJson(polygonJson)
    @addPolygonFromGooglePaths(paths, id)

  addPolygonFromGooglePaths: (paths, id = null) ->
    attributes = $.extend {}, @options.defaultPolygonOptions, paths: paths
    polygon = new google.maps.Polygon attributes
    polygon.setMap(@map)

    if id?
      @polygonsById[id] = polygon

  toggleFullscreen: ->
    @element.toggleClass('fullscreen')
    google.maps.event.trigger(@map, 'resize')



