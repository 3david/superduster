class Order < ActiveRecord::Base
  set_rgeo_factory_for_column(
    :area, 
    RGeo::Geographic.spherical_factory(:srid => 4326))

  validates_presence_of :area, :notes

  before_validation do
    puts "==============================="
    puts YAML::dump(area)
    puts "==============================="
  end

  def self.find_cities(term)
    self.select(:city) \
      .where("lower(city) like ?", "%#{term}%") \
      .group(:city) \
      .map(&:city)
  end
end
