window.Superduster =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  init: ->
    new Superduster.Routers.Orders()
    Backbone.history.start(pushState: true)

$(document).ready ->
  Superduster.init()
