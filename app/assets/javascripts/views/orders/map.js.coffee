class Superduster.Views.OrdersMap extends Backbone.View

  #template: JST['orders/index']
  initialize: ->
    @polygonsByCid = {}
    @initGoogleMap()
    @collection.on('reset', @renderPolygons, this)
    @collection.on('change:.hover', @onHoverChanged, this)
    @collection.on('change:.hidden', @onHiddenChanged, this)

  onHoverChanged: (order) ->
    polygon = @map.polygonsById[order.cid]
    console.error "No polygon with id #{order.cid}" unless polygon?
    if order.get('.hover')
      polygon.setOptions
        zIndex: 1000
        strokeColor: 'blue'
        fillColor: "#0000ff"
    else
      polygon.setOptions
        zIndex: 1
        strokeColor: 'red'
        fillColor: "#FF0000"

  onHiddenChanged: (order) ->
    polygon = @map.polygonsById[order.cid]
    visible = not order.get('.hidden')
    polygon.setOptions visible: visible

  renderPolygons: ->
    for order in @collection.models
      @map.addPolygonFromWkt(order.get('area'), order.cid)

  initGoogleMap: ->
    console.log "initializing google map!"
    @$el.googleMap allowDrawing: true
    @map = @$el.data('googleMap')
      
  demo_addLotsOfPolygons: ->
    points = [
      {lat: 26.55, lon: -70.18}
      {lat: 26.47, lon: -58.31}
      {lat: 20.42, lon: -60.51}
      {lat: 20.76, lon: -71.15}
      {lat: 26.55, lon: -70.18}
    ]
    
    index = 0
    for x in [1..50]
      for point in points
        point.lat = (Math.random() - 0.5) * 100
        point.lon = (Math.random() - 0.5) * 100
      @map.addPolygonFromJson(points, "polygon#{index}")
      index++
