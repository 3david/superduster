class Superduster.Views.OrdersList extends Backbone.View

  ungroupedTemplate: JST['orders/_orders_no_grouping']
  groupedTemplate: JST['orders/_orders_grouped']

  events:
    'click .order .toggle-visibility': 'toggleOrderVisibility'
    'click .group-title .toggle-visibility': 'toggleGroupVisibility'
    'mouseenter li.order': 'mouseEnter'
    'mouseleave li.order': 'mouseLeave'
    'click #group-by-city': 'groupByCity'
    'click #ungroup': 'showOrdersUngrouped'

  initialize: ->
    @collection.on('reset', @render, this)

  render: ->
    @showOrdersUngrouped()
    this

  mouseEnter: (event) ->
    @getOrder(event.target).set('.hover', true)

  mouseLeave: (event) ->
    @getOrder(event.target).unset('.hover')

  toggleOrderVisibility: (event) ->
    event.preventDefault()
    a = $(event.target)
    a.toggleClass('icons-eye icons-eye-disabled')
    li = $(event.target).closest('li')
    li.toggleClass('hidden-order')
    @getOrder(li).set('.hidden', li.hasClass('hidden-order'))

  toggleGroupVisibility: (event) ->
    event.preventDefault()
    a = $(event.target)
    groupLi = a.closest('li')
    groupLi.toggleClass('hidden-group')
    groupLi.find('.group-title .toggle-visibility') \
      .toggleClass('icons-eye icons-eye-disabled')
    isHidden = groupLi.hasClass('hidden-group')
    for li in groupLi.find('li.order')
      li = $(li).toggleClass('hidden-order', isHidden)
      @getOrder(li).set('.hidden', isHidden)

  getOrder: (li) ->
    @collection.getByCid $(li).data('cid')

  groupByCity: (event) ->
    event.preventDefault()
    console.log 'Grouping by city'
    @showOrdersGrouped('city')

  showOrdersUngrouped: (event) ->
    event.preventDefault() if event?
    $('#group-by dd').removeClass('active')
    $('#ungroup').addClass('active')
    html = @ungroupedTemplate(orders: @collection)
    $('#orders-list-container').html(html)
    #false

  showOrdersGrouped: (group) ->
    $('#group-by dd').removeClass('active')
    $("#group-by-#{group}").addClass('active')
    ordersByGroup = @collection.groupBy (row) -> row.get(group)
    html = @groupedTemplate(ordersByGroup: ordersByGroup)
    $('#orders-list-container').html(html)




