class Superduster.Views.OrderForm extends Backbone.View
  initialize: ->
    $('#order_city').autocomplete
      source: '/cities.json'
      minLength: 2
