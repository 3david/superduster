window.Geometry =

  Wkt:
    fromGooglePolygon: (google_polygon) ->
      coords = []
      path = google_polygon.getPath()
      i = 0
      while i < path.length
        coord = path.getAt(i++)
        coords.push "#{coord.lat()} #{coord.lng()}"
      return "POLYGON((#{coords.join(',')}))"

  GooglePolygonPaths:
    fromWkt: (wkt) ->
      wkt = wkt.replace(/POLYGON *\(\(/, '').replace('))', '')
      words = wkt.split(',')
      paths = []
      for word in words
        word = word.replace(/^ +/, '').replace(' +$', '')
        [lat, lon] = word.split(' ')
        lat = parseFloat(lat)
        lon = parseFloat(lon)
        latLng = new google.maps.LatLng(lat, lon)
        paths.push(latLng)
      return paths

    fromJson: (json) ->
      paths = []
      for point in json
        if $.isArray(point)
          paths.push new google.maps.LatLng(point[0], point[1])
        else
          paths.push new google.maps.LatLng(point.lat, point.lon)
      return paths

  GeoJSON:
    fromGooglePolygon: (google_polygon) ->
      geojson =
        type: 'Polygon'
        coordinates: []

      path = google_polygon.getPath()
      i = 0
      while i < path.length
        coord = path.getAt(i++)
        geojson.coordinates.push [coord.lat(), coord.lng()]

      geojson

