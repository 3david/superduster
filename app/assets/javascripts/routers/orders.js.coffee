class Superduster.Routers.Orders extends Backbone.Router
  routes:
    'orders': 'orders'
    'orders/new': 'newOrder'

  initialize: ->
    @collection = new Superduster.Collections.Orders()
    @collection.fetch()

  orders: ->
    window.coll = @collection
    listView = new Superduster.Views.OrdersList(collection: @collection, el: '#orders-panel')
    mapView = new Superduster.Views.OrdersMap(collection: @collection, el: '#map')

  newOrder: ->
    new Superduster.Views.OrderForm()
    new Superduster.Views.OrdersMap(collection: [], el: '#map')
