class Superduster.Collections.Orders extends Backbone.Collection
  url: '/orders'
  model: Superduster.Models.Order
