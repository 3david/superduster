class AddAreaToOrder < ActiveRecord::Migration
  def up
    add_column :orders, :area, :geometry
  end
  def down
    remove_column :orders, :area, :geometry
  end
end
