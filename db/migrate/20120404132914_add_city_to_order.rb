class AddCityToOrder < ActiveRecord::Migration
  def up
    add_column :orders, :city, :string
  end
  def down
    remove_column :orders, :city
  end
end
