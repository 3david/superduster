class ChangeOrderAreaToPolygon < ActiveRecord::Migration
  def up
    remove_column :orders, :area
    add_column :orders, :area, :polygon, :geographic => true
  end

  def down
    change_column :orders, :area, :geometry
  end
end
