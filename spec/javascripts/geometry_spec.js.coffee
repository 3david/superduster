class GoogleLatLngStub
  constructor: (@_lat, @_lng) ->
  lat: -> @_lat
  lng: -> @_lng
  
class GooglePolygonStub
  constructor: ->
    @_path = []
    @_path.getAt = (i) -> @[i]
  getPath: -> @_path
  addPoint: (lat, lng) ->
    @_path.push new GoogleLatLngStub(lat, lng)

describe "Geometry", ->
  describe "Wkt#fromGooglePolygon", ->
    it "converts correctly", ->
      polygon = new GooglePolygonStub()
      polygon.addPoint 10, 20
      polygon.addPoint 20, 30
      polygon.addPoint 40, 50
      wkt = Geometry.Wkt.fromGooglePolygon(polygon)
      expect(wkt).toEqual('POLYGON((10 20,20 30,40 50))')
