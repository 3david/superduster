require 'spec_helper'

describe Order do
  describe "#save" do
    describe "with valid polygon" do
      it "should save successfuly" do
        polygon = "POLYGON ((29.496987596535767 -75.87158203125, 28.69058765425071 -69.67529296875, 26.391869671769022 -72.22412109375, 26.391869671769022 -78.37646484375, 29.496987596535767 -75.87158203125))"
        order = Order.new area: polygon
        order.save()
        expected = RGeo::Geographic.spherical_factory(:srid => 4326).parse_wkt(polygon)
        order.area.should eql expected
      end
    end
  end

  describe "#find_intersecting(polygon)" do
    describe "with intersecting orders in the db" do
      it "should return results" do
        intersecting_polygon = POLYGON ((25.12539261151203 -79.16748046875, 25.522614647623293 -77.05810546875, 23.563987128451217 -77.58544921875, 25.12539261151203 -79.16748046875))
        non_intersecting_polygon = POLYGON ((37.82280243352756 -86.72607421875, 38.09998264736481 -83.51806640625, 36.56260003738545 -82.81494140625, 36.52729481454624 -86.41845703125, 37.82280243352756 -86.72607421875))

        test_polygon = POLYGON ((27.994401411046145 -78.99169921875, 28.806173508854776 -75.56396484375, 25.720735134412106 -72.35595703125, 23.241346102386135 -77.23388671875, 27.994401411046145 -78.99169921875))

        order = Order.new area: intersecting_polygon
        order.save()

        #Order.where('area ST_Intersect


      end
    end
  end
end
